package com.matritel.messagesender.controller;

import com.matritel.messagesender.model.Message;
import com.matritel.messagesender.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;
    //we don't need constructor because we don't need controller objects so that just inject the MessageService bean into the field


    @PostMapping(value = "messages")   //this annotation specified the http command
    @ResponseStatus(HttpStatus.OK)  //the httpStatus is an enum value means it has fixed value from you can choose sg
    @ResponseBody   //we have to tell that the message will be in the body of the JSON so that we use ResponseBody annotation
    public Message sendMessage(@RequestBody Message message){   //@RequestBody means that the method will be called just in that case when the JSON contains all necessary infos that must have a Message object
        return messageService.postMessage(message);
    }

    @GetMapping(value = "messages/{id}")  //curly bracket means it will be a variable in the url path
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Message getMessage(@PathVariable long id){    //if the id is appear in the url path then we will give back that specific message
        if (messageService.getMessage(id).isPresent()){
            return messageService.getMessage(id).get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Message is not found!");
    }

    @GetMapping(value = "messages")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<Message> getAllMessage(){
        return messageService.getAllMessage();
    }

    @DeleteMapping(value= "messages/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteMessage(@PathVariable long id){
        messageService.deleteMessage(id);
    }
}
