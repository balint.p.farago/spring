package com.matritel.messagesender.repository;

import com.matritel.messagesender.model.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {  // This also contains JPA and Hibernate implementations
    //we can implement CRUD or JPA repository interface but now create an own

/*  Message save(Message message);  // save is equal with post that's why we need to return a Message

    void deleteById(long id);



    //Optional class is actually a collection which only can contains 1 single element or nothing. BUT! It won't be thrown 'null'
    Optional<Message> findById(long id);
    */

    //This method will be implemented because a similar one is already in the in the extended interface. We just need to change the method return type and it will be implemented by Spring
    List<Message> findAll();  //findAll is equal with getAll


}
