package com.matritel.messagesender.service;

import com.matritel.messagesender.model.Message;
import com.matritel.messagesender.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

// the service annotation is a specific type of Component
@Service
public class MessageService {

    // to use methods from the MessageRepository we need to inject a MessageRepository object
    //the good practice is inject the dependency into the constructor
    private MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message postMessage(Message message){
        return messageRepository.save(message);
    }

    public Optional<Message> getMessage(long id){
        return messageRepository.findById(id);
    }

    public List<Message> getAllMessage(){
        return messageRepository.findAll();
    }

    public void deleteMessage(long id){
        messageRepository.deleteById(id);
    }
}
