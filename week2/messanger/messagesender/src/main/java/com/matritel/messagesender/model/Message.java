package com.matritel.messagesender.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Message {

    @Id
    @GeneratedValue
    private long id;

    @Column
    private String content;

    @Column
    private String consignee;  //receiver

}
