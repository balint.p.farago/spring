package com.matritel.recipe.food.cake.cakelayer;

import com.matritel.recipe.food.cake.ingredient.Sugar;

public abstract class CakeLayer {

    private Sugar sugar;

    public CakeLayer(Sugar sugar) {
        this.sugar = sugar;
    }

    public Sugar getSugar() {
        return sugar;
    }

    public abstract void showLayerRecipe();
}
