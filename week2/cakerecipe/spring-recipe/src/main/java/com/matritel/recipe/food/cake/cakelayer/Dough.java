package com.matritel.recipe.food.cake.cakelayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.matritel.recipe.food.cake.ingredient.*;

@Component
public class Dough extends CakeLayer{

    private Egg egg;
    private Flour flour;
    private Butter butter;
    private BakingSoda bakingSoda;

    @Autowired
    public Dough(@Qualifier(value = "sugarfordough")Sugar sugar, Egg egg, Flour flour, Butter butter, BakingSoda bakingSoda) {
        super(sugar);
        this.egg = egg;
        this.flour = flour;
        this.butter = butter;
        this.bakingSoda = bakingSoda;
    }

    @Override
    public void showLayerRecipe() {
        System.out.println("Dough{" +
                "egg=" + egg +
                ", flour=" + flour +
                ", butter=" + butter +
                ", bakingSoda=" + bakingSoda +
                ", sugar=" + super.getSugar() +
                '}');
    }
}
