package com.matritel.recipe.food.cake.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.matritel.recipe.food.cake.ingredient.*;

@Configuration
public class IngredientConfig {

    @Bean
    public BakingSoda bakingSoda() {
        return new BakingSoda(1);
    }

    @Bean
    public Butter butter() {
        return new Butter(250);
    }

    @Bean
    public ChocolateChip chocolateChip() {
        return new ChocolateChip(2);
    }

    @Bean
    public Egg egg() {
        return new Egg(6);
    }

    @Bean
    public Flour flour() {
        return new Flour(4);
    }

    @Bean
    public Milk milk() {
        return new Milk(4);
    }

    @Bean(name = "sugarfordough")
    public Sugar doughSugar() {
        return new Sugar(0.5);
    }

    @Bean(name = "sugarforpudding")
    public Sugar puddingSugar() {
        return new Sugar(1);
    }

    @Bean(name = "sugarfortopping")
    public Sugar toppingSugar() {
        return new Sugar(1.5);
    }

    @Bean
    public VanillaPuddingPowder vanillaPuddingPowder() {
        return new VanillaPuddingPowder(2);
    }

    @Bean
    public WhippedCream whippedCream() {
        return new WhippedCream(3);
    }
}
