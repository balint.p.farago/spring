package com.matritel.recipe.food.cake.ingredient;

public class Sugar {

    double amountInCup;

    public Sugar(double amountInCup) {
        this.amountInCup = amountInCup;
    }

    @Override
    public String toString() {
        return "Sugar{" +
                "amountInCup=" + amountInCup +
                '}';
    }
}
