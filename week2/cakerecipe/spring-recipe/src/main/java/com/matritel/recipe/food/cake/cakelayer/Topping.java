package com.matritel.recipe.food.cake.cakelayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.matritel.recipe.food.cake.ingredient.ChocolateChip;
import com.matritel.recipe.food.cake.ingredient.Sugar;
import com.matritel.recipe.food.cake.ingredient.WhippedCream;

@Component
public class Topping extends CakeLayer{

    private ChocolateChip chocolateChip;
    private WhippedCream whippedCream;

    @Autowired
    public Topping(@Qualifier(value = "sugarfortopping")Sugar sugar, ChocolateChip chocolateChip, WhippedCream whippedCream) {
        super(sugar);
        this.chocolateChip = chocolateChip;
        this.whippedCream = whippedCream;
    }

    @Override
    public void showLayerRecipe() {
        System.out.println("Topping{" +
                "chocolateChip=" + chocolateChip +
                ", whippedCream=" + whippedCream +
                ", sugar=" + super.getSugar() +
                '}');
    }
}
