package com.matritel.recipe.food.cake.cakelayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.matritel.recipe.food.cake.ingredient.Milk;
import com.matritel.recipe.food.cake.ingredient.Sugar;
import com.matritel.recipe.food.cake.ingredient.VanillaPuddingPowder;

@Component
public class Pudding extends CakeLayer{

    private VanillaPuddingPowder vanillaPuddingPowder;
    private Milk milk;

    @Autowired
    public Pudding(@Qualifier(value = "sugarforpudding")Sugar sugar, VanillaPuddingPowder vanillaPuddingPowder, Milk milk) {
        super(sugar);
        this.vanillaPuddingPowder = vanillaPuddingPowder;
        this.milk = milk;
    }

    @Override
    public void showLayerRecipe() {
        System.out.println("Pudding{" +
                "vanillaPuddingPowder=" + vanillaPuddingPowder +
                ", milk=" + milk +
                ", sugar=" + super.getSugar() +
                '}');
    }
}
