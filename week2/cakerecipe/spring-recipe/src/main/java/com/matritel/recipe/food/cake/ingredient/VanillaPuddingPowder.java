package com.matritel.recipe.food.cake.ingredient;

public class VanillaPuddingPowder {

    int amountInPackage;

    public VanillaPuddingPowder(int amountInPackage) {
        this.amountInPackage = amountInPackage;
    }

    @Override
    public String toString() {
        return "VanillaPuddingPowder{" +
                "amountInPackage=" + amountInPackage +
                '}';
    }
}
