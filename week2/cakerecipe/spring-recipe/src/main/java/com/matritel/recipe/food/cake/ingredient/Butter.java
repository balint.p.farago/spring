package com.matritel.recipe.food.cake.ingredient;

public class Butter {

    int amountInGram;

    public Butter(int amountInGram) {
        this.amountInGram = amountInGram;
    }

    @Override
    public String toString() {
        return "Butter{" +
                "amountInGram=" + amountInGram +
                '}';
    }
}
