package com.matritel.recipe.food.cake.ingredient;

public class WhippedCream {

    int amountInDeciliter;

    public WhippedCream(int amountInDeciliter) {
        this.amountInDeciliter = amountInDeciliter;
    }

    @Override
    public String toString() {
        return "WhippedCream{" +
                "amountInDeciliter=" + amountInDeciliter +
                '}';
    }
}
