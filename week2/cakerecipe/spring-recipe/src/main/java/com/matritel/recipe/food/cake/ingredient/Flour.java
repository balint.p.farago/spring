package com.matritel.recipe.food.cake.ingredient;

public class Flour {

    int amountInCup;

    public Flour(int amountInCup) {
        this.amountInCup = amountInCup;
    }

    @Override
    public String toString() {
        return "Flour{" +
                "amountInCup=" + amountInCup +
                '}';
    }
}
