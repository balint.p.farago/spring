package com.matritel.recipe.food.cake.ingredient;

public class Milk {

    int amountInDeciliter;

    public Milk(int amountInDeciliter) {
        this.amountInDeciliter = amountInDeciliter;
    }

    @Override
    public String toString() {
        return "Milk{" +
                "amountInDeciliter=" + amountInDeciliter +
                '}';
    }
}
