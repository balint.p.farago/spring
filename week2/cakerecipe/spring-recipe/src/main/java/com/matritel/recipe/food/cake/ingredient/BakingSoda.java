package com.matritel.recipe.food.cake.ingredient;

public class BakingSoda {

    int amountInPackage;

    public BakingSoda(int amountInPackage) {
        this.amountInPackage = amountInPackage;
    }

    @Override
    public String toString() {
        return "BakingSoda{" +
                "amountInPackage=" + amountInPackage +
                '}';
    }
}
