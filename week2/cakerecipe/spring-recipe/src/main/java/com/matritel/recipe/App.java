package com.matritel.recipe;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import com.matritel.recipe.food.CakeRecipe;

@ComponentScan
public class App {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(App.class);

        CakeRecipe cakeRecipe = context.getBean(CakeRecipe.class);

        cakeRecipe.showCakeRecipe();
    }
}
