package com.matritel.recipe.food.cake.ingredient;

public class ChocolateChip {

    int amountInPackage;

    public ChocolateChip(int amountInPackage) {
        this.amountInPackage = amountInPackage;
    }

    @Override
    public String toString() {
        return "ChocolateChip{" +
                "amountInPackage=" + amountInPackage +
                '}';
    }
}
