package com.matritel.recipe.food.cake.ingredient;

public class Egg {

    int amountInNumber;

    public Egg(int amountInNumber) {
        this.amountInNumber = amountInNumber;
    }

    @Override
    public String toString() {
        return "Egg{" +
                "amountInNumber=" + amountInNumber +
                '}';
    }
}
