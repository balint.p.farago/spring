package com.matritel.springbootsample.domain;

public class Pina {

    public int size;
    public int diagonal;

    public Pina() {
    }

    public Pina(int size, int diagonal) {
        this.size = size;
        this.diagonal = diagonal;
    }

}
