package com.matritel.springbootsample.controller;

import com.matritel.springbootsample.domain.Pina;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
public class ApplicationController {

    @GetMapping(value = "hello")
    public String getText(){
        return "Hello World";
    }

    @GetMapping(value = "pina")
    public Pina getPina(@RequestParam int size, @RequestParam int diagonal){
        return new Pina(size, diagonal);
    }
}
